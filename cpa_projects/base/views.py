from django.http import HttpResponse
# from django.template import Template, Context
from django.template.loader import get_template
from projects.models import Branch, Project


def index(request):
    # not optimal prefetch but good enough for now
    branches = Branch.objects.prefetch_related('manager',
                                               'manager__user',
                                               'projects',
                                               'projects__project_manager',
                                               'projects__project_manager__user',
                                               'projects__roles_in_project',
                                               'projects__roles_in_project__user',
                                               'projects__roles_in_project__role').all()
    template = get_template('base/home.html')
    return HttpResponse(template.render({'branches': branches}))
