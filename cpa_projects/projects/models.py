from django.db import models
from users.models import User


class BranchManager(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='branch_manager', verbose_name='Пользователь')

    def __str__(self):
        return self.user.first_name + ' ' + self.user.last_name

    class Meta:
        verbose_name = 'Руководитель направления'
        verbose_name_plural = 'Руководители направлениий'


class Branch(models.Model):
    name = models.CharField(max_length=200, verbose_name='Название направления')
    manager = models.ForeignKey(BranchManager, on_delete=models.CASCADE, related_name='branch',
                                null=True, blank=True, verbose_name='Руководитель')
    short_description = models.TextField( blank=True, verbose_name='Краткое описание')
    is_hidden = models.BooleanField(verbose_name='Скрытое направление (не отображается на сайте)', default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Направление'
        verbose_name_plural = 'Направления'


class Project(models.Model):
    branch = models.ForeignKey(Branch, on_delete=models.PROTECT, related_name='projects', verbose_name='Направление')
    name = models.CharField(max_length=200, verbose_name='Название проекта')
    short_description = models.TextField(verbose_name='Краткое описание')
    initiator = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Инициатор', null=True, blank=True)
    initiator_as_text = models.CharField(max_length=200, verbose_name='Инициатор текстом (если нет в системе)', blank=True)
    full_description = models.TextField(verbose_name='Проблема, которую призван решить проект или' +
                                                     ' возможность, которую можно использовать', blank=True)
    end_product = models.TextField(verbose_name='Описание конечного продукта', blank=True)
    current_state = models.TextField(verbose_name='Текущее состояние проекта', blank=True)
    semester_end_state = models.TextField(verbose_name='Состояние в конце семестра', blank=True)
    timeline = models.TextField(verbose_name='Цели и задачи с привязкой ко времени', blank=True)
    partners = models.TextField(verbose_name='Партнёры тесктом', blank=True)
    is_hidden = models.BooleanField(verbose_name='Скрытый проект (не отображается на сайте)', default=False)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'

    # TODO: default initiator as project manager
    # def save(self, *args, **kwargs):
    #     if not (self.initiator or self.initiator_as_text):
    #         self.initiator = ProjectManagerself.project_manager
    #     super(Project, self).save(*args, **kwargs)


class Role(models.Model):
    name = models.CharField(max_length=200, unique=True, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Роль'
        verbose_name_plural = 'Роли'


class RoleInProject(models.Model):
    project = models.ForeignKey(Project, on_delete=models.CASCADE, related_name='roles_in_project',
                                verbose_name='Роли в проекте')
    role = models.ForeignKey(Role, on_delete=models.CASCADE, related_name='roles_in_project',
                             blank=True, null=True, verbose_name='Роли в проекте')
    user = models.ForeignKey(User, on_delete=models.PROTECT, related_name='user_projects',
                             blank=True, null=True, verbose_name='Пользователь')
    user_name_text = models.CharField(max_length=200, null=True, blank=True,
                                      verbose_name='Имя, фамилия, курс, школа ДВФУ участника текстом')  # если нет привязанного пользователя
    prerequisites = models.TextField(null=True, blank=True,
                                     verbose_name='Требования к участнику проекта (знания или направления обучения)')
    description = models.TextField(null=True, blank=True,
                                   verbose_name='Содержание работы по проекту (что участник будет делать, что изучать)')

    def __str__(self):
        if self.user is not None:
            username = self.user.username
        elif self.user_name_text:
            username = self.user_name_text
        else:
            username = ''
        return username + ' - ' + str(self.role) + ' в проекте ' + self.project.name

    class Meta:
        verbose_name = 'Роль в проекте'
        verbose_name_plural = 'Роли и участники проекта'


class ProjectManager(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT, verbose_name='Пользователь', blank=True, null=True)
    project = models.OneToOneField(Project, on_delete=models.CASCADE, related_name='project_manager',
                                   verbose_name='Руководитель проекта', unique=True)
    user_name_text = models.CharField(max_length=200, null=True, blank=True,
                                      verbose_name='Имя, фамилия руководителя проекта текстом')
    user_email_text = models.CharField(max_length=200, null=True, blank=True,
                                       verbose_name='Электронная почта руководителя проекта текстом')
    is_vacant = models.BooleanField(verbose_name='Руководитель требуется', default=False)
    vacancy_description = models.TextField(null=True, blank=True,
                                           verbose_name='Требования к руководителю проекта (если есть вакансия)')

    def __str__(self):
        if self.user is not None:
            username = self.user.username
        elif self.user_name_text:
            username = self.user_name_text
        else:
            username = ''
        return username + ' - Руководитель проекта ' + self.project.name

    class Meta:
        verbose_name = 'Руководитель проекта'
        verbose_name_plural = 'Руководители проекта'

