from django.apps import AppConfig, apps


class ProjectsConfig(AppConfig):
    name = 'projects'
    verbose_name = 'Проекты'
