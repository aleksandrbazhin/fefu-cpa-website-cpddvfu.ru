from django.contrib import admin
from django.forms import TextInput, Textarea
from django.db import models
from rules.contrib.admin import ObjectPermissionsModelAdmin
from .models import Branch, Project, Role, RoleInProject, BranchManager, ProjectManager


admin.site.site_header = "Центр проектной деятельности ДВФУ - управление";
admin.site.site_title = "Центр проектной деятельности ДВФУ"

admin.site.register(Role, ObjectPermissionsModelAdmin)


class ProjectManagerInline(admin.StackedInline):
    model = ProjectManager
    max_num = 1
    extra = 1
    can_delete = True
    editable_fields = []

    fieldsets = (
        (None, {
            'fields': ('user',)
        }),
        ('Описание руководителя текстом (если нет пользователя в системе)', {
            'classes': ('collapse',),  # скрытые поля
            'fields': ('user_name_text', 'user_email_text',),
        }),
        ('Описание вакансии руководителя', {
            'classes': ('collapse',),  # скрытые поля
            'fields': ('is_vacant', 'vacancy_description', ),
        }),
    )

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 80})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 80})},
    }


class RoleInProjectInline(admin.StackedInline):
    model = RoleInProject
    extra = 1

    fields = ('role', 'user_name_text', 'prerequisites', 'description', )

    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 80})},
        models.TextField: {'widget': Textarea(attrs={'rows': 2, 'cols': 80})},
    }


    # readonly_fields = ('change_form_link',)


class ProjectAdmin(ObjectPermissionsModelAdmin):
    list_display = ('name', 'branch', )
    list_filter = ('branch',)
    inlines = (ProjectManagerInline, RoleInProjectInline, )
    ordering = ('branch', 'name',)
    fieldsets = (
        (None, {
            'fields': ('branch', 'name', 'short_description', 'is_hidden')
        }),
        ('Полное описание проекта', {
            'classes': (), #'collapse',), # скрытые поля
            'fields': ('full_description', 'end_product', ),
        }),
    )
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 80})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 80})},
    }


admin.site.register(Project, ProjectAdmin)
admin.site.register(BranchManager)


class BranchManagerInline(admin.TabularInline):
    model = ProjectManager
    max_num = 1
    extra = 1
    can_delete = False
    editable_fields = []


class ProjectInBranchInline(admin.StackedInline):
    model = Project
    extra = 1
    fields = ('name', )


class BranchAdmin(ObjectPermissionsModelAdmin):
    fields = ('name', "short_description", "is_hidden", "manager", )
    inlines = (ProjectInBranchInline, )
    formfield_overrides = {
        models.CharField: {'widget': TextInput(attrs={'size': 80})},
        models.TextField: {'widget': Textarea(attrs={'rows': 4, 'cols': 80})},
    }


admin.site.register(Branch, BranchAdmin)

