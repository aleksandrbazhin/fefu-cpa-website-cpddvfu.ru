import rules


@rules.predicate
def is_branch_manager(user, branch):
    # TODO: do not check on every admin query
    if branch is None:
        return True
    if branch.manager is None:
        return False
    return user == branch.manager.user

@rules.predicate
def is_project_branch_manager(user, project):
    if project is None:
        return True
    if project.project_manager is None or project.branch.manager is None \
            or user == project.branch.manager.user is None:
        return False
    return user == project.branch.manager.user


rules.add_rule('view_branch', is_branch_manager)
rules.add_rule('change_branch', is_branch_manager)


is_supermanager = rules.is_superuser | rules.is_group_member('top_manager')

# rules.add_perm('projects', rules.always_allow)
rules.add_perm('projects.view_project', is_project_branch_manager | is_supermanager)
rules.add_perm('projects.add_project', is_project_branch_manager | is_supermanager)
rules.add_perm('projects.change_project', is_project_branch_manager | is_supermanager)
rules.add_perm('projects.delete_project', is_project_branch_manager | is_supermanager)


rules.add_perm('projects.view_branch', is_branch_manager | is_supermanager)
rules.add_perm('projects.add_branch', is_supermanager)
rules.add_perm('projects.change_branch', is_branch_manager | is_supermanager)
rules.add_perm('projects.delete_branch', is_supermanager)

rules.add_perm('projects.view_role', rules.always_allow)
rules.add_perm('projects.add_role', rules.always_allow)
rules.add_perm('projects.change_role', rules.always_allow)
rules.add_perm('projects.delete_role', rules.always_allow)
