from django.http import HttpResponse
from django.template.loader import get_template
from .models import Project
from django.db.models import Q


def index(project, id):
    # not optimal prefetch but good enough for now
    project = Project.objects.get(pk=id)
    participants = project.roles_in_project.filter(Q(user__isnull=False) |
                                                   (Q(user_name_text__isnull=False) & ~Q(user_name_text__exact='')))
    vacancies = project.roles_in_project.filter(Q(user__isnull=True) &
                                                (Q(user_name_text__isnull=True) | Q(user_name_text__exact='')))
    template = get_template('projects/project.html')
    return HttpResponse(template.render({'project': project, 'vacancies': vacancies, 'participants': participants}))


