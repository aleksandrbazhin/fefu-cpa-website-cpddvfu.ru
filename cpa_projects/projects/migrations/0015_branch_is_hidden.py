# Generated by Django 2.1 on 2018-09-17 16:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('projects', '0014_auto_20180918_0033'),
    ]

    operations = [
        migrations.AddField(
            model_name='branch',
            name='is_hidden',
            field=models.BooleanField(default=False, verbose_name='Скрытое направление (не отображается на сайте)'),
        ),
    ]
