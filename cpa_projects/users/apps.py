from django.apps import AppConfig, apps


class UsersConfig(AppConfig):
    name = 'users'
    verbose_name = 'Пользователи и права'

