from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    # is_confirmed = models.BooleanField('Подтвержден', default=False)
    is_confirmed_student = models.BooleanField('Является ли студентом', default=False)
    # middle_name = models.CharField(max_length=100)

    def __str__(self):
        return self.first_name + ' ' + self.last_name

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'


class Student(models.Model):
    user = models.OneToOneField(User, primary_key=True, on_delete=models.CASCADE)
    group = models.CharField(max_length=10)
